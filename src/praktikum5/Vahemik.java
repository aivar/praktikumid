package praktikum5;

import lib.TextIO;

public class Vahemik {

	public static void main(String[] args) {

		System.out.println("Sisesta miinimum:");
		int min = TextIO.getlnInt();
		System.out.println("Sisesta maksimum:");
		int max = TextIO.getlnInt();
		kasutajaSisestus(min, max);
	}

    public static int kasutajaSisestus(int min, int max) {
        while (true) {
            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
            int sisestus = TextIO.getlnInt();
            if (sisestus >= min && sisestus <= max) {
                return sisestus;
            } else {
                System.out.println("Vigane sisestus! Proovi uuesti.");
            }
        }
    }
    
}
