package praktikum2;

import lib.TextIO;

public class KaheNrKorrutis {

	public static void main(String[] args) {

		int esimene;
		System.out.println("Palun sisesta esimene arv");
		esimene = TextIO.getlnInt();
		int teine;
		System.out.println("Palun sisesta teine arv");
		teine = TextIO.getlnInt();

		int korrutis = esimene * teine;
		System.out.println("Kahe arvu korrutis on " + korrutis + ".");
	}
}
