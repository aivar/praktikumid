package praktikum2;

import lib.TextIO;

public class NimePikkus {

	public static void main(String[] args) {

		String nimi;

		System.out.println("Sisesta nimi");
		nimi = TextIO.getlnString();
		int nimePikkus = nimi.length();
		System.out.println("Nime pikkus on " + nimePikkus + " tähte.");

	}

}
