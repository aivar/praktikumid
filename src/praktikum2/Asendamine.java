package praktikum2;

import lib.TextIO;

public class Asendamine {

	public static void main(String[] args) {
		
		String nimi;
		
		System.out.println("Sisesta nimi");
		nimi = TextIO.getlnString();
		
		String valenimi;
		valenimi = nimi.replace('a', '_');
		//valenimi = nimi.replace('A', '_');
		//nimi.replaceAll(regex, replacement);
		System.out.println("Asendamisega on nimi " + valenimi + ".");

	}

}
