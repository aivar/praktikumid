package praktikum6;

import praktikum5.Meetodid;

public class KullJaKiri {

	public static void main(String[] args) {
		
		int kasutajaRaha = 1;

		while (kasutajaRaha != 0) {

			System.out.println("Tere tulemast mängu!");
			System.out.println("Sisesta panus (max 25)");
			int panuseSuurus = Meetodid.kasutajaSisestus(1, 25);
			kasutajaRaha -= panuseSuurus;

			int myndiVise = AraArvamisMang.suvalineArv(0, 1);
			if (myndiVise == 0) {
				System.out.println("Võitsid, saad raha topelt tagasi!");
				kasutajaRaha += panuseSuurus * 2;
			} else {
				System.out.println("Kaotasid");
			}
			System.out.println("Sul on " + kasutajaRaha + " raha.");
		}
	}

}
