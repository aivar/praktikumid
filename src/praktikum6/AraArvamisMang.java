package praktikum6;

import lib.TextIO;

public class AraArvamisMang {

	public static void main(String[] args) {

		int arvutiArv = suvalineArv(1, 100);

		while (true) {
			System.out.println("Sisesta arv ühest sajani:");
			int kasutajaArv = TextIO.getlnInt();
			if (kasutajaArv == arvutiArv) {
				System.out.println("Õige!");
				break;
			} else if (kasutajaArv < arvutiArv) {
				System.out.println("Vale! Arv on suurem.");
			} else {
				System.out.println("Vale! Arv on väiksem.");
			}

		}

	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;
	}

}
