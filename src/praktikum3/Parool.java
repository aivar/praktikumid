package praktikum3;

import lib.TextIO;

public class Parool {

	public static void main(String[] args) {

		int sisestusteArv = 0;
		while (true) {
			String sisend;
			String parool = "fafeafge523";

			System.out.println("Sisesta parool");
			sisend = TextIO.getln();

			if (parool.equals(sisend)) {
				System.out.println("Õige parool!");
				break;
			} else {
				System.out.println("Vale parool");
				sisestusteArv = sisestusteArv + 1;
			}

			if (sisestusteArv >= 3) {
				System.out.println("Sisestasid kolm korda vale parooli, aitab küll!");
				break;
			}
		}
	}
}
