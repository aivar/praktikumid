package praktikum3;

import lib.TextIO;

public class TehisIntellekt {

	public static void main(String[] args) {
		
		int vanusa;
		int vanusb;
		int vanusevahe;
		
		System.out.println("Kui vana sa oled?");
		vanusa = TextIO.getInt();
		System.out.println("Kui vana su kaaslane on?");
		vanusb = TextIO.getInt();
		vanusevahe = Math.abs(vanusa-vanusb);
		
		if (vanusevahe > 10){
			System.out.println("Perverssus!");
		} else if(vanusevahe > 5){
			System.out.println("Kahtlane värk!");
		} else {
			System.out.println("Sobib!");
		}

	}

}
